## Información general del proyecto:

Keep It Safe: Save your accounts sería un plugin o add-on para navegadores web 
(como pueden ser Chrome, Firefox, etc.) el cual se encargaría de cerrar sesiones
que se hayan dejado abiertas al salir de la última pestaña o enviar un recordatorio
en el que figuren dichas sesiones. Se incluirían las páginas más comunes, es decir,
redes sociales punteras y servicios de correo electrónico
(Gmail, Hotmail, Facebook, Instagram…) con el fin de proteger y mejorar la privacidad
del usuario. También tenemos planeada una traducción al inglés, para conseguir una mayor
expansión del proyecto.
Se puede aplicar al uso personal, teniendo un recordatorio de cerrar la sesión, o al 
uso en institutos, oficinas u ordenadores públicos de, por ejemplo, bibliotecas, 
haciendo que las sesiones se cierren automáticamente mediante un horario programable.
Con este proyecto queremos facilitar la protección del usuario ante la distribución
de cuentas personales y lograr así un grado más alto de privacidad digital.

This project is licensed under the GNU Public License v3, you can review the terms of this in the LICENSE file.